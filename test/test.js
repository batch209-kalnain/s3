const chai = require('chai');
const { assert } = require('chai');

// Import and use chai-http to allow chai to send requests to our server
const http = require('chai-http');
chai.use(http);

describe("api test suite users",() => {
	it("test api get users is running",(done) => {
		// .request() method is used from chai to create an http request to the given server.
		// .get("/endpoint") is used to run/access a get method route.
		// .end() which is used to access the response from the route. It has an anonymous function as an argument that receives 2 objects, the err or the response objects.
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err,res) => {
			// .isDefined is an assertion that the given data is not undefined. It's like a shortcut to .notEqual(typeof data,undefined).
			assert.isDefined(res);
			done();
		})

		// done() method is used to tell chai http when the test is done.
	})

	it("test api get users returns array",(done) => {
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err,res) => {
			// res.body contains the body of the response. The data sent from res.send().
			// console.log(res.body);

			// isArray() is an assertion that the given data is an array.
			assert.isArray(res.body);
			done();
		})
	})

	it("test api get users array first object name is John",(done) => {
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err,res) => {
			// console.log(res.body[0]);
			assert.equal(res.body[0].name,"John");
			done();
		})
	})

	it("test api users to check/confirm the last item in the array of object is not undefined",(done) => {
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err,res) => {
			// console.log(res.body[res.body.length-1]);
			assert.notEqual(res.body[res.body.length-1],undefined);
			// assert.equal(res.body[res.body.length-1],undefined);
			done();
		})
	})

	it("test api post users returns 400 if no name",(done) => {
		// .post() which is used by chai http to access a post method route.
		// .type() which is used to tell chai that the request body is going yo be stringified as json.
		// .send() which is used to send the request body.
		chai.request("http://localhost:4000")
		.post('/users')
		.type("json")
		.send({
			age:31,
			username:"irene91"
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	/* ACTIVITY SOLUTION */

	it("test api post users is running",(done) => {
		chai.request("http://localhost:4000")
		.post('/users')
		.end((err,res) => {
			assert.isDefined(res)
			done();
		})
	})

	it("test api post users returns 400 if no username",(done) => {
		chai.request("http://localhost:4000")
		.post('/users')
		.type("json")
		.send({
			name:"Alteir",
			age:21
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it("test api post users returns 400 if no age",(done) => {
		chai.request("http://localhost:4000")
		.post('/users')
		.type("json")
		.send({
			name:"Alexander",
			username:"theking01"
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})


});

describe("api test suite products",() => {
	it("test api get products is running",(done) => {
		chai.request("http://localhost:4000")
		.get("/products")
		.end((err,res) => {
			assert.isDefined(res);
			done();
		})
	});

	it("test api get products returns array",(done) => {
		chai.request("http://localhost:4000")
		.get("/products")
		.end((err,res) => {
			assert.isArray(res.body);
			done();
		})
	});

	it("test api get products to check if the first item in the array is an object",(done) => {
		chai.request("http://localhost:4000")
		.get("/products")
		.end((err,res) => {
			assert.isObject(res.body[0]);
			// assert.isNotObject(res.body[0]);
			done();
		})
	});
});