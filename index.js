const express = require('express');
const app = express();
const port = process.env.PORT || 4000;

app.use(express.json());

//let users = ["John","Johnson","Smith"];
let users = [
	{
		name:"John",
		age:18,
		username:"johnsmith99"
	},
	{
		name:"Johnson",
		age:21,
		username:"johnson1991"	
	},
	{
		name:"Smith",
		age:19,
		username:"smithMike12"
	}
];

let products = [
	{
		name:"Cleanser",
		price:499,
		isActive:true
	},
	{
		name:"Sunscreen",
		price:399,
		isActive:true
	},
	{
		name:"Toner",
		price:599,
		isActive:true
	}
];

app.get('/users',(req,res) => {
	return res.send(users);
})

app.post('/users',(req,res) => {
	// Add a simple if statement, that if the request body does not have a name property, we will send an error message along with a 400 http status code.

	// .hasOWnProperty() returns a boolean if the property name passed exists or does not exists in the given object.
	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error:"Bad Request - missing required parameter NAME"
		})
	}

	if(!req.body.hasOwnProperty("age")){
		return res.status(400).send({
			error:"Bad Request - missing required parameter AGE"
		})
	}

	if(!req.body.hasOwnProperty("username")){
		return res.status(400).send({
			error:"Bad Request - missing required parameter USERNAME"
		})
	}
})

app.get('/products',(req,res) => {
	return res.send(products);
})


app.listen(port,() => console.log("Express is running at port " + port))